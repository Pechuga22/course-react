import React from 'react';
import ReactDOM from 'react-dom';
import Index from './pages/index.jsx';
// import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <Index/>
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
