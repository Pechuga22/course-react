import React, { useState, useEffect } from "react";
import Call from './config/axios'

function App(props) {

  const [pokemons, setPokemons] = useState([])

  useEffect(() => {
    getPokemon()
  }, []);

const getPokemon = async () =>
{
  const res = await Call('GET', '/pokemon/?offset=0&limit=40', false, null)
  setPokemons(res.data.results)
}

const getInfoFromPokemon = async (url) =>
{
  let id = url.split('/')[6]
  props.history.push(`/pokemon/${id}`)
}

  return (
    <div className="App">
      {pokemons &&
        pokemons?.map((item, index) => (
          item?.url &&
          <p key={index} onClick={() => getInfoFromPokemon(item?.url)}>
            {item?.name}
          </p>
        ))
      }
    </div>
  );
}

export default App;
