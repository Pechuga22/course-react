import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import App from '../App'
import InfoPokemon from '../pages/infoPokemon'

export default function MainRoute()
{
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={App}/>
        <Route exact path="/pokemon/:id" component={InfoPokemon}/>
        <Route path="*">
          <h1>404</h1>
        </Route>
      </Switch>
    </Router>
  )
}