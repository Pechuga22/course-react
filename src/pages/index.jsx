import React from 'react'
import MainRoute from '../routes/main.route';

export default function Index() {
  return (
    <div>
      <MainRoute/>
    </div>
  )
}
