/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
// import Abilities from '../components/pokemonsAbilities';
import { useParams } from 'react-router-dom'
import Abilities from '../components/pokemonsAbilities';
import Call from '../config/axios';

const InfoPokemon = (props) => 
{
  const [pokemon, setPokemon] = useState(null)
  const { id } = useParams()
  useEffect(() => {
    getInfo()
  }, [id]);

  const getInfo = async () =>
  {
    const res = await Call('GET', `/pokemon/${id}`, false, null,)
    setPokemon(res.data)
  }

  return (
    <div>
      <button onClick={() => props.history.push('/')}>back</button>
      {pokemon ? <Abilities pokemon={pokemon} setPokemon={setPokemon} />
        : 'Cargando...'
      }
    </div>
  );
}

export default InfoPokemon;
